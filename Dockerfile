﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["src/OpenwrksNetflixReviews.API/OpenwrksNetflixReviews.API.csproj", "api/"]
RUN dotnet restore "api/OpenwrksNetflixReviews.API.csproj"
COPY . .

FROM build AS publish
RUN dotnet publish -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "OpenwrksNetflixReviews.API.dll"]
