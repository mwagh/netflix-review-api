﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using OpenwrksNetflixReviews.Data.Models;
using OpenwrksNetflixReviews.Persistence;
using Xunit;

namespace OpenwrksNetflixReviews.API.Tests.Persistence
{
    public class NetflixReviewsRepositoryTests
    {
        [Fact]
        public void HasReview_WhenReviewExists_ReturnsTrue()
        {
            var testShowId = "testShowId";
            var data = new List<KeyValuePair<string, Review>>
            {
                new (testShowId, new Review())
            };
            var subject = InitialiseSubject(data);
            
            Assert.True(subject.HasReview(testShowId));
        }

        [Fact]
        public void HasReview_WhenNoReviewExists_ReturnsFalse()
        {
            var subject = InitialiseSubject(Array.Empty<KeyValuePair<string, Review>>());
            Assert.False(subject.HasReview("something"));
        }

        [Fact]
        public void Get_WhenReviewExists_ReturnsData()
        {
            var testShowId = "testShowId";
            var review = new Review { Score = 1, Description = "test" };
            var data = new List<KeyValuePair<string, Review>>
            {
                new (testShowId, review)
            };
            
            var subject = InitialiseSubject(data);
            
            var result = subject.Get(testShowId);
            Assert.Equal(review.Score, result.Score);
            Assert.Equal(review.Description, result.Description);
        }
        
        [Fact]
        public void Get_WhenNoReviewExists_ReturnsNull()
        {
            var testShowId = "testShowId";
            var subject = InitialiseSubject(Array.Empty<KeyValuePair<string, Review>>());
            var result = subject.Get(testShowId);
            Assert.Null(result);
        }

        [Fact]
        public void AddReview_WithValidShowId_SavesData()
        {
            var testShowId = "testShowId";
            var expectedReview = new Review
            {
                Score = 1,
                Description = "test"
            };
            
            var dict = new ConcurrentDictionary<string, Review>(new List<KeyValuePair<string, Review>>());
            var subject = new NetflixReviewsRepository(dict);
            
            subject.AddReview(testShowId, expectedReview);
            Assert.True(dict.ContainsKey(testShowId));
            Assert.True(dict.TryGetValue(testShowId, out var actualReview));
            Assert.Equal(actualReview, expectedReview);
        }
        
        [Fact]
        public void AddReview_WhenReviewExistsAlready_ReplacesExistingReview()
        {
            var testShowId = "existingTestShowId";
            var existingReview = new Review
            {
                Score = 1,
                Description = "test 1"
            };

            var newReview = new Review
            {
                Score = 2,
                Description = "test 2"
            };
            
            var dict = new ConcurrentDictionary<string, Review>(new List<KeyValuePair<string, Review>>
            {
                new (testShowId, existingReview)
            });
            
            var subject = new NetflixReviewsRepository(dict);
            
            subject.AddReview(testShowId, newReview);
            Assert.True(dict.ContainsKey(testShowId));
            Assert.True(dict.TryGetValue(testShowId, out var actualReview));
            Assert.Equal(actualReview, newReview);
        }

        private NetflixReviewsRepository InitialiseSubject(IEnumerable<KeyValuePair<string, Review>> data)
        {
            var dict = new ConcurrentDictionary<string, Review>(data);
            return new NetflixReviewsRepository(dict);
        }
    }
}