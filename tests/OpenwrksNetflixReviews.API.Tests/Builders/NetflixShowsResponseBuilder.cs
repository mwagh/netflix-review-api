﻿using System.Linq;
using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.API.Tests.Builders
{
    public class NetflixShowsResponseBuilder
    {
        private readonly NetflixShowsResponse _instance;

        public NetflixShowsResponseBuilder()
        {
            _instance = new NetflixShowsResponse
            {
                Data = new NetflixShow[] {},
                Links = null,
                Pagination = null
            };
        }

        public NetflixShowsResponseBuilder AddShow(NetflixShow show)
        {
            _instance.Data = _instance.Data.Append(show).ToArray();
            return this;
        }

        public NetflixShowsResponseBuilder SetLinks(Links links)
        {
            _instance.Links = links;
            return this;
        }

        public NetflixShowsResponseBuilder SetPagination(Pagination pagination)
        {
            _instance.Pagination = pagination;
            return this;
        }

        public NetflixShowsResponse Build()
        {
            return _instance;
        }
    }
}