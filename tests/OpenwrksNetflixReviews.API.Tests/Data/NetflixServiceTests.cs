﻿using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using OpenwrksNetflixReviews.API.Tests.Builders;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.Models;
using OpenwrksNetflixReviews.Persistence;
using Xunit;

namespace OpenwrksNetflixReviews.API.Tests.Data
{
    public class NetflixServiceTests
    {
        private readonly INetflixService _serviceMock;
        private readonly INetflixReviewsRepository _repositoryMock;
        private readonly NetflixService _subject;

        public NetflixServiceTests()
        {
            _serviceMock = Substitute.For<INetflixService>();
            _repositoryMock = Substitute.For<INetflixReviewsRepository>();
            _subject = new NetflixService(_serviceMock, _repositoryMock);
        }

        [Fact]
        public async Task GetShows_WhenNoReviewExists_CallsBaseImplementationAndChecksForReview()
        {
            var testShowId = "testShow";
            var from = 123;
            var limit = 456;

            var expectedResponse = new NetflixShowsResponseBuilder()
                .AddShow(new NetflixShow { Id = testShowId })
                .Build();

            _repositoryMock.HasReview(Arg.Is<string>(x => x == testShowId))
                .Returns(false);
            
            _repositoryMock.Get(Arg.Any<string>())
                .Returns(new Review());
            
            _serviceMock.GetShows(Arg.Any<int>(), Arg.Any<int>())
                .Returns(expectedResponse);
            
            var result = _subject.GetShows(from, limit);
            
            await _serviceMock.Received()
                .GetShows(Arg.Is<int>(x => x == from), Arg.Is<int>( x => x == limit));
            
            _repositoryMock.Received()
                .HasReview(Arg.Is<string>(x => x == testShowId));
            
            Assert.True(result.IsCompletedSuccessfully);
        }
        
        [Fact]
        public async Task GetShows_WhenReviewExists_CallsBaseImplementationAndDecorates()
        {
            var testShowId = "testShow";
            var from = 123;
            var limit = 456;
            var expectedReview = new Review
            {
                Score = 3,
                Description = "test review"
            };

            var expectedResponse = new NetflixShowsResponseBuilder()
                .AddShow(new NetflixShow { Id = testShowId })
                .Build();

            _repositoryMock.HasReview(Arg.Is<string>(x => x == testShowId))
                .Returns(true);
            
            _repositoryMock.Get(Arg.Any<string>())
                .Returns(expectedReview);
            
            _serviceMock.GetShows(Arg.Any<int>(), Arg.Any<int>())
                .Returns(expectedResponse);
            
            var result = await _subject.GetShows(from, limit);
            
            await _serviceMock.Received()
                .GetShows(Arg.Is<int>(x => x == from), Arg.Is<int>( x => x == limit));
            
            _repositoryMock.Received()
                .HasReview(Arg.Is<string>(x => x == testShowId));

            _repositoryMock.Received()
                .Get(Arg.Is<string>(x => x == testShowId));
            
            Assert.Equal(expectedReview.Score, result.Data.FirstOrDefault().Review.Score);
            Assert.Equal(expectedReview.Description, result.Data.FirstOrDefault().Review.Description);
        }

        [Fact]
        public async Task GetShow_WhenNoReviewExists_CallsBaseImplementationAndChecksForReview()
        {
            var testShowId = "testShow";
            var expectedShow = new NetflixShow
            {
                Id = testShowId
            };
            
            _repositoryMock.HasReview(Arg.Is<string>(x => x == testShowId))
                .Returns(false);
            
            _repositoryMock.Get(Arg.Any<string>())
                .Returns(new Review());
            
            _serviceMock.GetShow(Arg.Any<string>())
                .Returns(expectedShow);
            
            var result = _subject.GetShow(testShowId);
            
            await _serviceMock.Received()
                .GetShow(Arg.Is<string>(x => x == testShowId));
            
            _repositoryMock.Received()
                .HasReview(Arg.Is<string>(x => x == testShowId));

            _repositoryMock.DidNotReceiveWithAnyArgs()
                .Get(Arg.Any<string>());
            
            Assert.True(result.IsCompletedSuccessfully);
        }
        
        [Fact]
        public async Task GetShow_WhenReviewExists_CallsBaseImplementationAndDecorates()
        {
            var testShowId = "testShow";
            var expectedReview = new Review
            {
                Score = 3,
                Description = "test review"
            };

            var expectedShow = new NetflixShow
            {
                Id = testShowId
            };
            
            _repositoryMock.HasReview(Arg.Is<string>(x => x == testShowId))
                .Returns(true);
            
            _repositoryMock.Get(Arg.Any<string>())
                .Returns(expectedReview);
            
            _serviceMock.GetShow(Arg.Any<string>())
                .Returns(expectedShow);
            
            var result = await _subject.GetShow(testShowId);
            
            await _serviceMock.Received()
                .GetShow(Arg.Is<string>(x => x == testShowId));
            
            _repositoryMock.Received()
                .HasReview(Arg.Is<string>(x => x == testShowId));

            _repositoryMock.Received()
                .Get(Arg.Is<string>(x => x == testShowId));
            
            Assert.Equal(expectedReview.Score, result.Review.Score);
            Assert.Equal(expectedReview.Description, result.Review.Description);
        }
    }
}