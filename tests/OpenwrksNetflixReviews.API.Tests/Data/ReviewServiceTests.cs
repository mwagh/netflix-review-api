﻿using System.Threading.Tasks;
using NSubstitute;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.Models;
using OpenwrksNetflixReviews.Persistence;
using Xunit;

namespace OpenwrksNetflixReviews.API.Tests.Data
{
    public class ReviewServiceTests
    {
        private readonly INetflixReviewsRepository _repositoryMock;
        private readonly ReviewService _subject;

        public ReviewServiceTests()
        {
            _repositoryMock = Substitute.For<INetflixReviewsRepository>();
            _subject = new ReviewService(_repositoryMock);
        }

        [Fact]
        public async Task SaveReview_WithValidParams_Succeeds()
        {
            var testShowId = "testShowId";
            var expectedReview = new Review
            {
                Score = 1,
                Description = "test review"
            };
            
            _repositoryMock.When(x => x.AddReview(Arg.Any<string>(), Arg.Any<Review>()))
                .Do(x => { });
            
            _subject.SaveReview(testShowId, expectedReview);
            
            _repositoryMock.Received()
                .AddReview(Arg.Is<string>(x => x == testShowId), Arg.Is<Review>(x => x == expectedReview));
        }
    }
}