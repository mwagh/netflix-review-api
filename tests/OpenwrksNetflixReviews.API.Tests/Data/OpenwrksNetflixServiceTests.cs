﻿using System;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using OpenwrksNetflixReviews.API.Tests.Substitutes;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.External;
using OpenwrksNetflixReviews.Data.Models;
using Xunit;

namespace OpenwrksNetflixReviews.API.Tests.Data
{
    public class OpenwrksNetflixServiceTests
    {
        private OpenwrksNetflixService _subject;
        private IHttpClientFactory _factory;
        private HttpClient _client;

        public OpenwrksNetflixServiceTests()
        {
            _factory = Substitute.For<IHttpClientFactory>();
        }


        [Fact]
        public async Task GetShows_WithValidParams_ReturnsOk()
        {
            var responseString = JsonSerializer.Serialize(new OpenwrksNetflixShowsResponse());
            Setup(responseString, HttpStatusCode.OK);
            var response = await _subject.GetShows(1, 1);
            Assert.IsType<NetflixShowsResponse>(response);
        }
        
        [Fact]
        public async Task GetShow_WithValidParams_ReturnsOk()
        {
            var responseString = JsonSerializer.Serialize(new NetflixShow());
            Setup(responseString, HttpStatusCode.OK);
            var response = await _subject.GetShow("");
            Assert.IsType<NetflixShow>(response);
        }

        private void Setup(string responseString, HttpStatusCode statusCode)
        {
            _client = new HttpClient(new SubstituteHttpMessageHandler(responseString, statusCode))
            {
                BaseAddress = new Uri("http://localhost")
            };

            _factory
                .CreateClient(Arg.Any<string>())
                .Returns(_client);
            
            _subject = new OpenwrksNetflixService(_factory);
        }
    }
}