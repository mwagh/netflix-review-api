using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using OpenwrksNetflixReviews.Controllers;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.Models;
using Xunit;

namespace OpenwrksNetflixReviews.API.Tests
{
    public class NetflixShowsControllerTests
    {
        private INetflixService _netflixServiceMock;
        private IReviewService _reviewServiceMock;
        private NetflixShowsController _subject;
        
        public NetflixShowsControllerTests()
        {
            _netflixServiceMock = Substitute.For<INetflixService>();
            _reviewServiceMock = Substitute.For<IReviewService>();
            _subject = new NetflixShowsController(_netflixServiceMock, _reviewServiceMock);
        }

        #region GetShows
        
        [Fact]
        public async Task GetShows_WithValidParams_ReturnsOk()
        {
            _netflixServiceMock.GetShows(Arg.Any<int>(), Arg.Any<int>())
                .Returns(new NetflixShowsResponse());

            var response = await _subject.GetShows(new NetflixShowsRequest
            {
                PageLimit = 1,
                From = 1
            });
            Assert.IsType<OkObjectResult>(response);
        }
        
        [Fact]
        public async Task GetShows_WithInvalidParams_ReturnsBadRequest()
        {
            _netflixServiceMock
                .GetShows(Arg.Any<int>(), Arg.Any<int>())
                .Returns(new NetflixShowsResponse());

            _subject.ModelState.AddModelError("key", "error message");
            
            var response = await _subject.GetShows(new NetflixShowsRequest
            {
                PageLimit = 0,
                From = 0
            });
            Assert.IsType<BadRequestResult>(response);
        }
        
        #endregion

        #region GetShow

        [Fact]
        public async Task GetShow_WithValidParams_ReturnsOk()
        {
            _netflixServiceMock
                .GetShow(Arg.Any<string>())
                .Returns(new NetflixShow());
            
            var response = await _subject.GetShow("showId");
            Assert.IsType<OkObjectResult>(response);
        }
        
        [Fact]
        public async Task GetShow_WithInvalidParams_ReturnsBadRequest()
        {
            _netflixServiceMock.GetShow(Arg.Any<string>())
                .Returns(new NetflixShow());

            _subject.ModelState.AddModelError("key", "error message");
            
            var response = await _subject.GetShow("showId");
            Assert.IsType<BadRequestResult>(response);
        }
        #endregion
    }
}