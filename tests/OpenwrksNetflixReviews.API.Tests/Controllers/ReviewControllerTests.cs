﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using OpenwrksNetflixReviews.Controllers;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.Models;
using Xunit;

namespace OpenwrksNetflixReviews.API.Tests
{
    public class ReviewControllerTests
    {
        private readonly IReviewService _reviewServiceMock;
        private readonly ReviewController _subject;

        public ReviewControllerTests()
        {
            _reviewServiceMock = Substitute.For<IReviewService>();
            _subject = new ReviewController(_reviewServiceMock);
        }

        #region SaveReview

        [Fact]
        public async Task SaveReview_WithValidParams_ReturnsNoContent()
        {
            _reviewServiceMock
                .SaveReview(Arg.Any<string>(), Arg.Any<Review>());
        
            var response = await _subject.SaveReview(new ReviewRequest());
            
            Assert.IsType<NoContentResult>(response);
        }
        
        [Fact]
        public async Task SaveReview_WithInvalidParams_ReturnsBadRequest()
        {
            _reviewServiceMock
                .SaveReview(Arg.Any<string>(), Arg.Any<Review>());
            
            _subject.ModelState.AddModelError("key", "error message");
            
            var response = await _subject.SaveReview(new ReviewRequest());
            
            Assert.IsType<BadRequestResult>(response);
        }

        #endregion
    }
}