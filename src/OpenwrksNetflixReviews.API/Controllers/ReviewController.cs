﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReviewController: ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpPost]
        public async Task<IActionResult> SaveReview([FromBody] ReviewRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            _reviewService.SaveReview(request.ShowId, request.Review);
            return NoContent();
        }
    }
}