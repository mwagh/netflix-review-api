﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NetflixShowsController : ControllerBase
    {
        private readonly INetflixService _netflixService;
        private readonly IReviewService _reviewService;

        public NetflixShowsController(INetflixService netflixService, IReviewService reviewService)
        {
            _netflixService = netflixService;
            _reviewService = reviewService;
        }

        [HttpGet]
        public async Task<IActionResult> GetShows([FromQuery] NetflixShowsRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var shows = await _netflixService.GetShows(request.From, request.PageLimit);
            return Ok(shows);
        }

        [HttpGet("/show/{showId}")]
        public async Task<IActionResult> GetShow(string showId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            
            var shows = await _netflixService.GetShow(showId);
            return Ok(shows);
        }
    }
}