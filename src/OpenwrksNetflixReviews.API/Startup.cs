using System;
using System.Net.Http;
using IdentityModel.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OpenwrksNetflixReviews.Data;
using OpenwrksNetflixReviews.Persistence;

namespace OpenwrksNetflixReviews
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebAppliOpenwrksNetflixReviews", Version = "v1" });
            });
            
            
            services.AddAccessTokenManagement(options =>
            {
                options.Client.Clients.Add("OpenwrksNetflix", new ClientCredentialsTokenRequest
                {
                    Method = HttpMethod.Post,
                    ClientCredentialStyle = ClientCredentialStyle.AuthorizationHeader,
                    AuthorizationHeaderStyle = BasicAuthenticationHeaderStyle.Rfc2617,
                    Address = Configuration["Endpoints:OpenwrksNetflixAuth"],
                    ClientId = Configuration["Authentication:OpenwrksNetflix:ClientId"],
                    ClientSecret = Configuration["Authentication:OpenwrksNetflix:ClientSecret"],
                    Scope = Configuration["Authentication:OpenwrksNetflix:Scope"],
                });
            });
            
            services.AddClientAccessTokenClient("netflix", configureClient: client =>
            {
                client.BaseAddress = new Uri(Configuration["Endpoints:OpenwrksNetflix"]);
            });
            
            services.AddSingleton<INetflixReviewsRepository, NetflixReviewsRepository>();
            services.AddTransient<INetflixService, OpenwrksNetflixService>();
            services.AddTransient<IReviewService, ReviewService>();
            services.Decorate<INetflixService, NetflixService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAppliOpenwrksNetflixReviews"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            }
    }
}