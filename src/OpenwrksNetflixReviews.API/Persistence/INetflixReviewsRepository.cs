﻿using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Persistence
{
    public interface INetflixReviewsRepository
    {
        bool HasReview(string showId);
        Review Get(string showId);
        void AddReview(string showId, Review review);
    }
}