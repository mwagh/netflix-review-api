﻿using System;
using System.Collections.Concurrent;
using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Persistence
{
    public class NetflixReviewsRepository : INetflixReviewsRepository
    {
        private readonly ConcurrentDictionary<string, Review> _reviews;

        public NetflixReviewsRepository(ConcurrentDictionary<string, Review> dictionary = default)
        {
            _reviews = dictionary ?? new ConcurrentDictionary<string, Review>();
        }

        public bool HasReview(string showId)
        {
            return _reviews.ContainsKey(showId);
        }

        public Review Get(string showId)
        {
            if (!_reviews.TryGetValue(showId, out var review))
            {
                return null;
            }

            return review;

        }

        public void AddReview(string showId, Review review)
        {
            if (_reviews.ContainsKey(showId))
            {
                var oldReview = Get(showId);
                if (!_reviews.TryUpdate(showId, review, oldReview))
                {
                    throw new Exception($"Unable to update value for key: {showId}");
                }
            }
            else
            {
                if (!_reviews.TryAdd(showId, review))
                {
                    throw new Exception($"Unable to add value for key: {showId}");
                }
            }
        }
    }
}