﻿using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Data
{
    public interface IReviewService
    {
        void SaveReview(string showId, Review review);
    }
}