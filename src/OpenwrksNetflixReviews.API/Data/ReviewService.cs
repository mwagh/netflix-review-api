﻿using OpenwrksNetflixReviews.Data.Models;
using OpenwrksNetflixReviews.Persistence;

namespace OpenwrksNetflixReviews.Data
{
    public class ReviewService : IReviewService
    {
        private readonly INetflixReviewsRepository _repository;

        public ReviewService(INetflixReviewsRepository repository)
        {
            _repository = repository;
        }

        public void SaveReview(string showId, Review review)
        {
            _repository.AddReview(showId, review);
        }
    }
}