﻿using System.Collections.Generic;

namespace OpenwrksNetflixReviews.Data.Models
{
    public class NetflixShowsResponse
    {
        public NetflixShow[] Data { get; set; }
        public Links Links { get; set; }
        public Pagination Pagination { get; set; }
    }

    public class Links
    {
        public string Next { get; set; }
        public string Prev { get; set; }
    }

    public class Pagination
    {
        public int Page { get; set; }
        public int Limit { get; set; }
        public int Total { get; set; }
    }
}