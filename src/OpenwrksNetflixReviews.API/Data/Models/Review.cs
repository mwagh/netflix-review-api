﻿using System.ComponentModel.DataAnnotations;

namespace OpenwrksNetflixReviews.Data.Models
{
    public class Review
    {
        [Required]
        [Range(0,5)]
        public int Score { get; set; }
        
        [Required]
        [MinLength(1)]
        [MaxLength(255)]
        public string Description { get; set; }
    }
}