﻿namespace OpenwrksNetflixReviews.Data.Models
{
    public class ReviewRequest
    {
        public string ShowId { get; set; }
        public Review Review { get; set; }
    }
}