﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace OpenwrksNetflixReviews.Data.Models
{
    public class NetflixShowsRequest
    {
        [Required]
        [Range(1, 100)]
        [JsonPropertyName("pageLimit")]
        public int PageLimit { get; set; }
        
        [Required]
        [Range(1, int.MaxValue)]
        [JsonPropertyName("from")]
        public int From { get; set; }
    }
}