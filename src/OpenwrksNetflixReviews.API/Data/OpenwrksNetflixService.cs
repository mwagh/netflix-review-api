﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Mapster;
using OpenwrksNetflixReviews.Data.External;
using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Data
{
    public class OpenwrksNetflixService : INetflixService
    {
        private HttpClient _client;
        
        public OpenwrksNetflixService(IHttpClientFactory clientFactory)
        {
            _client = clientFactory.CreateClient("netflix");

        }

        public async Task<NetflixShowsResponse> GetShows(int from = 1, int limit = 20)
        {
            var query = $"?page={from}&limit={limit}";
            var apiResponse = await _client.GetAsync("v1/shows" + query);
            apiResponse.EnsureSuccessStatusCode();
            var responseContent = await apiResponse.Content.ReadFromJsonAsync<OpenwrksNetflixShowsResponse>();
            return responseContent.Adapt<NetflixShowsResponse>();
        }

        public async Task<NetflixShow> GetShow(string showId)
        {
            var apiResponse = await _client.GetAsync($"v1/shows/{showId}");
            apiResponse.EnsureSuccessStatusCode();
            var responseContent =  await apiResponse.Content.ReadFromJsonAsync<OpenwrksNetflixShow>();
            return responseContent.Adapt<NetflixShow>();
        }
    }
}