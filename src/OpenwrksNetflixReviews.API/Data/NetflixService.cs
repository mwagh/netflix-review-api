﻿using System.Linq;
using System.Threading.Tasks;
using OpenwrksNetflixReviews.Data.Models;
using OpenwrksNetflixReviews.Persistence;

namespace OpenwrksNetflixReviews.Data
{
    public class NetflixService : INetflixService
    {
        private readonly INetflixService _service;
        private readonly INetflixReviewsRepository _repository;

        public NetflixService(INetflixService _service, INetflixReviewsRepository repository)
        {
            this._service = _service;
            _repository = repository;
        }

        public async Task<NetflixShowsResponse> GetShows(int from = 1, int limit = 20)
        {
            var response = await _service.GetShows(from, limit);
            for (int i = 0; i < response.Data.Length; i++)
            {
                var showId = response.Data[i].Id;
                if (_repository.HasReview(showId))
                {
                    response.Data[i].Review = _repository.Get(showId);
                }
            }

            return response;
        }

        public async Task<NetflixShow> GetShow(string showId)
        {
            var show = await _service.GetShow(showId);
            if (_repository.HasReview(show.Id))
            {
                show.Review = _repository.Get(show.Id);
            }

            return show;
        }
    }
}