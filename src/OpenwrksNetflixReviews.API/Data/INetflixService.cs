﻿using System.Threading.Tasks;
using OpenwrksNetflixReviews.Data.Models;

namespace OpenwrksNetflixReviews.Data
{
    public interface INetflixService
    {
        Task<NetflixShowsResponse> GetShows(int from = 1, int limit = 20);
        Task<NetflixShow> GetShow(string showId);
    }
}